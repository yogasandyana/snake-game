// Deklarasi ukuran board game
const canvas  = document.getElementById('board');
const context = canvas.getContext("2d");
const box     = 32;

// Load gambar background dan apel
const bg    = new Image();
const apple = new Image();
bg.src      = "img/ground.png";
apple.src   = "img/food.png";

// Deklarasi ular
let snake = [];

// Posisi awal ular
snake[0] = {
    x : 9 * box,
    y : 10 * box,
}

// Deklarasi posisi makanan ular (apel)
let food = {
    x : Math.floor(Math.random() * 17 + 1) * box,
    y : Math.floor(Math.random() * 15 + 3) * box,
}

console.log(food.x)

// Deklarasi skor
let score = 0;

// Menentukan pergerakan ular
let direction;
document.addEventListener("keydown", updateDirection);

// Fungsi yang dijalankan saat menekan tombol arah
function updateDirection(e) {
    let key = e.keyCode;

    if (key == 37 && direction != "RIGHT") {
        direction = "LEFT";
    } else if (key == 38 && direction != "DOWN") {
        direction = "UP";
    } else if (key == 39 && direction != "LEFT") {
        direction = "RIGHT";
    }else if (key == 40 && direction != "UP") {
        direction = "DOWN";
    }
}

// Fungsi jika ular menabrak batas canvas
function collision(head, array) {
    for (let i = 0; i < array.length; i++) {
        if (head.x == array[i].x && head.y == array[i].y) {
            return true;
        }
    }
    return false;
}

function update() {
    context.drawImage(bg, 0, 0);
    for (let i = 0; i < snake.length; i++) {
        context.fillStyle = (i == 0)? "blue" : "green";     // Pemberian warna kepada ular jika memkan apel akan diberikan warna hijau dan kepala ular berwarna biru
        context.fillRect(snake[i].x, snake[i].y, box, box); 
        context.strokeStyle = "grey";
        context.strokeRect(snake[i].x, snake[i].y, box, box);
    }

    context.drawImage(apple, food.x, food.y); // Menempatkan makanan ular baru

    let snakeX = snake[0].x;
    let snakeY = snake[0].y;
    if (direction == "LEFT") snakeX -= box;
    if (direction == "UP") snakeY -= box;
    if (direction == "RIGHT") snakeX += box;
    if (direction == "DOWN") snakeY += box;

    if (snakeX == food.x && snakeY == food.y) {
        score++;
        food = {
            x : Math.floor(Math.random() * 17 + 1) * box,
            y : Math.floor(Math.random() * 15 + 3) * box,
        }
    } else {
        snake.pop();
    }

    let newHead = {
        x : snakeX,
        y : snakeY
    }

    // Kondisi jika menabrak box atau ular sudah berukuran box
    if (snakeX < box || snakeX > 17 * box || snakeY < 3 * box || snakeY > 17 * box || collision(newHead, snake)) {
        clearInterval(game)
    }

    snake.unshift(newHead);

    context.fillStyle = "white";
    context.font = "45px Poppins";
    context.fillText(score, 2 * box, 1.6 * box);
}

let game = setInterval(update, 150); // Kecepatan ular



